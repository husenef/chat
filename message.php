
<!--  START CUSTOM CSS -->
<style>
    #bottom-message-fixed{
        bottom: 0;
        position: fixed;
        right: 200px;
    }
    #bottom-message-fixed .message-containers .message-child{
        background-color: #a30c1b;
        border: 1px solid #999999;
        display: block;
        float: right;
        height: 300px;
        margin-right: 10px;
        width: 250px;
        z-index: 100
    }

    #bottom-message-fixed .message-containers .message-child .top{
        background-color: #3e4853;
        height: 35px;
        width: 100%;
        color:#ffffff;
        padding:5px;
        float:left;
    }

    #bottom-message-fixed .message-containers .message-child .top a{
        color:#ffffff;
    }

    #bottom-message-fixed .message-containers .message-child .top a:hover{
        color:#f3f3f3;
    }	
    #bottom-message-fixed .message-containers .message-child .top i{
        cursor: pointer;
        margin: 0;
        padding: 5px;
    }

    #bottom-message-fixed .message-containers .message-child .mid{
        height: 200px;
        width: 100%;
        padding:5px;
        float:left;
        overflow: auto;
    }

    #bottom-message-fixed .message-containers .message-child .bottom{
        background-color: #f5f5f5;
        height: 65px;
        width: 100%;
        color:#000000;
        float:left;
    }

    #bottom-message-fixed .message-containers .message-child .bottom #boost_message{
        border-radius:0px;
        width:100%;
        resize: none;
        height: 100%;
    }
    
    #sidebar-message-fixed{
        background-color: #990911;
        border-left: 1px solid #757575;
        height: 100%;
        position: fixed;
        right: -200px;
        top: 0;
        width: 200px;
    }
    #sidebar-message-fixed	.ward{
        background-color: #2e353d;
        border: 1px solid #6f6f6f;
        color: #fff;
        cursor: pointer;
        height: 30px;
        left: -30px;
        padding: 6px 7px;
        position: absolute;
        top: 45%;
        width: 30px;
    }

    #sidebar-message-fixed .ward:hover{
        color:#ededed;
        background-color:#1c232b;
    }

    #sidebar-message-fixed .message-container,#bottom-message-fixed .message-containers{
        display: block;
        height: 100%;
        width: 100%;
    }

    #sidebar-message-fixed .message-container .message-container-child{
        width:100%;
        height:43px;
        float:left;
        display:block;
        cursor:pointer;
        border-bottom:1px solid #777777;
        padding: 5px;
    }

    #sidebar-message-fixed .message-container .message-container-child img{
        width:32px;
        height:32px;
        border:1px solid #919191;
        float:left;
        margin-left: 20px;
    }

    #sidebar-message-fixed .message-container .message-container-child #icone-online,#sidebar-message-fixed .message-container .message-container-child #icone-offline{
        color: #ffffff;
        left: 0;
        padding-left: 5px;
        padding-top: 10px;
        position: absolute;
    }

    #sidebar-message-fixed .message-container .message-container-child span{
        width:120px;
        height:22px;
        text-align:left;
        float:left;
        color:#ffffff;
        margin:5px;
    }
    /*update chat*/
    #bottom-message-fixed .message-containers .message-child .mid .chat{
        display: block;
        padding: 5px 3px;
    }
    .left{
        text-align: left;
    }
    .right{
        text-align: right;
    }
</style>

<!--   END CUSTOM CSS  -->

<section id="sidebar-message-fixed">
    <i class="icon-forward ward" id="toggle_me"  style="display:none;"></i>
    <i class="icon-backward ward" id="toggle_me_out"></i>
    <div class="message-container" id="message-container"></div>
</section>
<section id="bottom-message-fixed">
    <div class="message-containers" id="message-containers" ></div>
</section>