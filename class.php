<?php

session_start();
include 'config.php';


/* * ***	Disable This When Development Mode Done  *****
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  /*****	Disable This When Development Mode Done  **** */

class Globalboost {

    var $uid;
    var $dbs;

    /* configurasi database */
//    var $host = 'localhost';
//    var $user = 'blglbost';
//    var $pass = 'DSNRyrwbnznBe5NU';
//    var $db = "blglbost_pr0";
    var $host = '';
    var $user = '';
    var $pass = '';
    var $db = "";

    function __construct($uid) {

        $this->uid = $uid;
        $this->host = host;
        $this->user = user;
        $this->pass = pass;
        $this->db = db;
        $this->pdo_secure();
    }

    protected function pdo_secure() {
        try {
            $this->dbs = new PDO("mysql:host=$this->host;dbname=$this->db", "$this->user", "$this->pass");
        } catch (PDOException $e) {
            print $e->getMessage();
            die();
        }
    }

    public function _GetUsers($ufid) {
        $_exec = $this->dbs->prepare('SELECT * from user WHERE user_id=:user_id');
        $_exec->bindParam(':user_id', $ufid);
        $_exec->execute();
        return $_exec->fetchAll(PDO::FETCH_ASSOC);
    }

    public function _GetFriendship() {
        $_exec = $this->dbs->prepare('SELECT * from users_friendship WHERE uid=:uid or ufid=:ufid');
        $_exec->bindParam(':uid', $this->uid);
        $_exec->bindParam(':ufid', $this->uid);
        $_exec->execute();
        return $_exec->fetchAll(PDO::FETCH_ASSOC);
        //echo ($_exec->fetchColumn() > 0 ? json_encode() : '404');
    }

    public function _CheckFriendship($ufid, $status = "") {
        $_exec = $this->dbs->prepare('SELECT COUNT(*) FROM users_friendship WHERE (uid=:uid and ufid=:ufid) or (uid=:uids and ufid=:ufids)');
        $_exec->bindParam(':uid', $ufid);
        $_exec->bindParam(':ufid', $this->uid);
        $_exec->bindParam(':uids', $this->uid);
        $_exec->bindParam(':ufids', $ufid);
        $_exec->execute();
        if ($status == "")
            return ($_exec->fetchColumn() > 0 ? true : false);
        else
            return $_exec->fetchColumn();
    }

    public function _SetFriendship($ufid) {
        if (!$this->_CheckFriendship($ufid)) {
            $_exec = $this->dbs->prepare("INSERT INTO users_friendship (uid, ufid) VALUES (:uid, :ufid)");
            $_exec->bindParam(':uid', $ufid);
            $_exec->bindParam(':ufid', $this->uid);
            if ($_exec->execute()) {
                return '200';
            } else {
                return '404';
            }
        }
    }

    public function _UnSetFriendship($ufid) {
        if ($this->_CheckFriendship($ufid)) {
            $_exec = $this->dbs->prepare('DELETE FROM users_friendship WHERE (uid=:uid and ufid=:ufid) or (uid=:uids and ufid=:ufids)');
            $_exec->bindParam(':uid', $ufid);
            $_exec->bindParam(':ufid', $this->uid);
            $_exec->bindParam(':uids', $this->uid);
            $_exec->bindParam(':ufids', $ufid);
            if ($_exec->execute()) {
                return '200';
            } else {
                return '404';
            }
        }
    }

    public function _Logged() {
        $_exec = $this->dbs->prepare('UPDATE user set last_logged=:udates WHERE user_id=:uid');
        $_exec->bindParam(':uid', $this->uid);
        $_exec->bindParam(':udates', strtotime('now'));
        $_exec->execute();
    }

    /*
     * name : _SendMessage
     * descrition : save chat message
     */

    function _SendMessage($data = array()) {
        if (empty($data) || !is_array($data))
            return false;
        $toId = base64_decode($data['data']['id']);
        $message = $data['data']['value'];
        $_exec = $this->dbs->prepare("INSERT INTO users_message (from_id, to_id,message) VALUES (:from_id, :to_id,:message)");
        $_exec->bindParam(':from_id', $this->uid);
        $_exec->bindParam(':to_id', $toId);
        $_exec->bindParam(':message', $message);
        if ($_exec->execute())
            echo json_encode(array('status' => 1, 'm' => $message, 'c' => $toId));
        else
            echo json_encode(array('status' => 0, 'm' => 'error di server', 'c' => $toId));
    }

    /*
     * @name : _getChat
     * @descrition : get chat history
     * @variable : (int) $owner & (int) $frend
     */

    public function _getChat($owner = null, $frend = null) {
        if (empty($owner))
            return FALSE;
        if (empty($frend))
            return FALSE;

        $query = "SELECT * FROM `users_message` WHERE (`from_id` = '$owner' OR `from_id` ='$frend') AND (`to_id` = '$owner' OR `to_id`='$frend') ORDER BY `users_message`.`id` ASC";
        $_exec = $this->dbs->prepare($query);
//        $_exec->bindParam(':owner', $owner);
//        $_exec->bindParam(':frend', $frend);
        $return = '';
        if ($_exec->execute()) {
            foreach ($_exec->fetchAll(PDO::FETCH_ASSOC)as $chat) {
                $class = ($chat['from_id'] == $owner) ? 'left' : 'right';
                $return .= "<span data-id='boot-" . $chat['from_id'] . "' class='chat $class'>" . $chat['message'] . "</span>";
            }
            return $return;
        } else {
            return $return;
        }
    }

    /*
     * name : _saveImageChat
     * descrition : save chat img
     */

    function _saveImageChat($data = array()) {
        if (empty($data) || !is_array($data))
            return false;
        echo json_encode($data);
    }

}

if (isset($_SESSION['userid'])) {
    $cls = new Globalboost($_SESSION['userid']);
    if ($_POST['status'] == 'chkfriendship') {
        if ($cls->_CheckFriendship($_POST['ufid'], 'check') == 0) {
            echo '0';
        } else {
            echo '1';
        }
    } else if ($_POST['status'] == 'unsetfriendship') {
        echo $cls->_UnSetFriendship($_POST['ufid']);
    } else if ($_POST['status'] == 'setfriendship') {
        echo $cls->_SetFriendship($_POST['ufid']);
    } else if ($_POST['status'] == 'getfriendship') {
        $cls->_Logged();
        $_MYSQLData = '';
        foreach ($cls->_GetFriendship() as $_listFriendship) {
            if ($_listFriendship['uid'] != $_SESSION['userid']) {
                if ($_MYSQLData == '') {
                    $_MYSQLData = "'" . $_listFriendship['uid'] . "'";
                } else {
                    $_MYSQLData .=",'" . $_listFriendship['uid'] . "'";
                }
            } else if ($_listFriendship['ufid'] != $_SESSION['userid']) {
                if ($_MYSQLData == '') {
                    $_MYSQLData = "'" . $_listFriendship['ufid'] . "'";
                } else {
                    $_MYSQLData .=",'" . $_listFriendship['ufid'] . "'";
                }
            }
        }

        if ($_MYSQLData):
            $query = "SELECT a.user_id,a.username,a.last_logged,b.photos FROM user a inner join user_profile b on a.user_id=b.user_id where a.user_id in (" . $_MYSQLData . ")";
            $_exec = $cls->dbs->query("$query");

            $_exec->execute();
            $_DataFriend = '';
            foreach ($_exec->fetchAll(PDO::FETCH_ASSOC) as $_DataFriends) {
                $_DataFriend .= '<div class="message-container-child" id="boost-friendship" uid="boost-' . $_DataFriends['user_id'] . '">' . ($_DataFriends['last_logged'] < strtotime('now - 45 second') ? '<i class="icon-eye-close" id="icone-offline"></i>' : '<i class="icon-eye-open" id="icone-online"></i>') . '
                                            <img src="' . ($_DataFriends['photos'] == '' ? 'https://globalboo.st/uploads/no-image.png' : 'https://dev.globalboo.st/uploads/' . $_DataFriends['photos']) . '">
                                            <span>' . $_DataFriends['username'] . '</span>
                                        </div>';
            }
        else:
            $_DataFriend = '';
        endif;

        if ($_DataFriend != '')
            echo $_DataFriend;
        else
            echo '<div class="message-container-child">You have not added any friends</div>';
    }else if ($_POST['status'] == 'showchat') {

        $_data = '';
        $userId = $_SESSION['userid'];
        $frendId = str_replace('boost-', '', $_POST['uid']);
        foreach ($cls->_GetUsers($frendId) as $_Users) {
            $id = base64_encode(str_replace('boost-', '', $_POST['uid']));
            $message = $cls->_getChat($userId, $frendId);

            $_data = '<div id="' . $_POST['uid'] . '" class="message-child">';
            $_data.='<div class="top"><a href="https://globalboo.st/+/' . $_Users['username'] . '/" class="pull-left">' . $_Users['username'] . '</a><i class="icon-remove pull-right" div-id="' . $_POST['uid'] . '"></i></div>';
            $_data.= '<div class="mid">' . $message . '</div>';
            $_data.='<div class="bottom">';

//            $_data .="<form id='upload' method='post' action='upload.php' enctype='multipart/form-data'>";
//            $_data .="<div id='drop'>";
//            $_data .='<textarea id = "boost_message" data-id = "' . $id . '" onkeyup = "isEnterPressed(this,event)"></textarea>';
//            $_data .="<a>Browse</a>";
//            $_data .="<input type='file' name='upl' multiple />";
//            $_data .="</div>";
//            $_data .= "</form>";
            $_data .='<textarea id="boost_message" data-id="' . $id . '" onkeyup="isEnterPressed(this,event)"></textarea>';
            $_data .= '<input id="fileupload-' . $frendId . '" type="file" name="files[]" multiple>';
            $_data .='<div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>';

            $_data.='</div></div>';
        }
        echo $_data;
    } else if ($_POST['status'] == 'send') {
        $cls->_SendMessage($_POST);
    } else if ($_POST['status'] == 'sendimage') {
        $cls->_saveImageChat($_POST);
    } else {
        
    }
}
?>













