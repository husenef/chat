
function uploadGan(id) {
    var ids = id.split('-');
    var theId = ids[1];
    var url = postUrl + 'upload/php/';
    var parent = $('#fileupload-' + theId).parents('.message-child');
    var mid = parent.find('.mid');
    $(function () {
        'use strict';
//       $('#fileupload-' + theId).addClass('husen');
        console.log("#fileupload-" + theId);
        $('#fileupload-' + theId).fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                $.each(data.files, function (index, file) {
                    $.post(postUrl + 'class.php', {
                        data: file.name, status: 'sendimage'
                    }, function (re) {
                        $('<span class="chat left"/>').text(file.name).appendTo(mid);
                    });
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
}