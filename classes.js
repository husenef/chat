/*
 * @version     : 1.0.0
 * @update      : - remove ur in $.post change with var postUrl
 *                - set postUrl in root htm <script>var postUrl = 'http://www.domain.com/pathClass/'</script>
 *                - update send message
 */

$(document.body).on('click', '#bottom-message-fixed .message-containers .message-child .top i', function () {
    $('#' + $(this).attr('div-id')).remove();
});

$(document.body).on('click', '#boost-friendship', function () {
    var boost_uid = $(this).attr('uid');
//    $.post("https://globalboo.st/classes/class.php", {status: 'showchat', uid: boost_uid}, function (data) {
//        $('#' + boost_uid).remove();
//        $('#message-containers').append(data);
//    });
    $.post(postUrl + "class.php", {status: 'showchat', uid: boost_uid}, function (data) {
        $('#' + boost_uid).remove();
        $('#message-containers').append(data);
        //run jquery upload
        setTimeout(function () {
            uploadGan(boost_uid);
        }, 100);

    });
});

$(document).ready(function () {

    setInterval(function () {
//        $.post("https://globalboo.st/classes/class.php", {status: 'getfriendship'}, function (data) {
        $.post(postUrl + "class.php", {status: 'getfriendship'}, function (data) {
            if (data != '404') {
                $('#message-container').empty();
                $('#message-container').append(data);
            }
        });
    }, 60000);

    if ($('#friendship_ids').val()) {
        var ufid = $('#friendship_ids').val();
        if (ufid != '0' || ufid != '') {
            $.post(postUrl + "class.php", {
                ufid: ufid, status: 'chkfriendship'
            }, function (data) {
                if (data == 0) {
                    $('#add_friendship').css('display', 'block');
                    $('#un_friendship').css('display', 'none');
                } else {
                    $('#add_friendship').css('display', 'none');
                    $('#un_friendship').css('display', 'block');
                }
            });
        }
    }

    $.post(postUrl + "class.php", {status: 'getfriendship'}, function (data) {
        if (data != '404') {
            $('#message-container').empty();
            $('#message-container').append(data);
        }
    });

    $('#add_friendship').click(function () {
        if (ufid != '0' || ufid != '') {
            $(this).attr('disabled', 'disabled');
            $.post(postUrl + "class.php", {
                ufid: ufid, status: 'setfriendship'
            }, function (data) {
                if (data == 200) {
                    $('#add_friendship').css('display', 'none');
                    $('#un_friendship').css('display', 'block');
                    $(this).removeAttr('disabled');
                } else {
                    $('#add_friendship').css('display', 'block');
                    $('#un_friendship').css('display', 'none');
                    $(this).removeAttr('disabled');
                }
            });
        } else
            $(this).removeAttr('disabled');
    });

    $('#un_friendship').click(function () {
        if (ufid != '0' || ufid != '') {
            $(this).attr('disabled', 'disabled');
            $.post(postUrl + "class.php", {
                ufid: ufid, status: 'unsetfriendship'
            }, function (data) {
                if (data == 200) {
                    $('#add_friendship').css('display', 'block');
                    $('#un_friendship').css('display', 'none');
                    $(this).removeAttr('disabled');
                } else {
                    $('#add_friendship').css('display', 'none');
                    $('#un_friendship').css('display', 'block');
                    $(this).removeAttr('disabled');
                }
            });
        } else
            $(this).removeAttr('disabled');
    });

    $('#toggle_me').click(function () {
        $('#sidebar-message-fixed').animate({
            right: "-200px"
        }, 'slow', function () {
            $('#toggle_me').hide('fast');
            $('#toggle_me_out').show('fast');
        });
    });

    $('#toggle_me_out').click(function () {
        $('#sidebar-message-fixed').animate({
            right: "0px"
        }, 'slow', function () {
            $('#toggle_me').show('fast');
            $('#toggle_me_out').hide('fast');
        });
    });


});

/* on enter press*/
function isEnterPressed(t, e) {
    var keycode = null;
    if (e != null) {
        if (window.event != undefined) {
            if (window.event.keyCode)
                keycode = window.event.keyCode;
            else if (window.event.charCode)
                keycode = window.event.charCode;
        } else {
            keycode = e.keyCode;
        }
    }
    if (keycode === 13) {
        var data = {
            value: t.value,
            id: t.getAttribute('data-id'),
        };
        t.value = '';

        send(data);
    }
    // return (keycode == 13);
}
var send = function (message) {
    console.log("getting progress");
    $.post(postUrl + "class.php", {
        data: message, status: 'send'
    }, function (r) {
        var ms = $.parseJSON(r);
        console.log(ms);
        if (ms.status) {
//            console.log(ms.c);
            $('#boost-' + ms.c).find('.mid').append("<span data-id='bost-" + ms.c + "' class='chat left'>" + ms.m + "<span>");
        }
    });
};
